import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopulationByAgeComponent } from './population-by-age.component';

describe('PopulationByAgeComponent', () => {
  let component: PopulationByAgeComponent;
  let fixture: ComponentFixture<PopulationByAgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopulationByAgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopulationByAgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
