import { Component, OnInit } from '@angular/core';
import { GraphDatabaseJsonserviceService } from '../services/graph-database-jsonservice.service';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material';
import { ChartConfigService } from '../services/chart-config-service.service';

@Component({
  selector: 'app-population-by-age',
  templateUrl: './population-by-age.component.html',
  styleUrls: ['./population-by-age.component.css'],
  providers: [GraphDatabaseJsonserviceService, ChartConfigService]
})

export class PopulationByAgeComponent implements OnInit {

  private CATEGORY_API_URL
  = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/population-by-age/1pPOMrGJUBfRjFzFfjjfG-k7XuEUO11FKQIiota4XFKo';
  populaceData: any[] = [];

  yearSelection: any[];
  categorySelection: any[];

  selectedYear: string;
  selectedCategory: string;

  chartDataSets = [];
  chartLabels: any[];
  chartType: string = 'horizontalBar';
  comparisonChartType: string = 'line';
  chartLegend: boolean = true;
  chartData: any[];

  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private jsonDB: GraphDatabaseJsonserviceService, private configData: ChartConfigService) {

  }

  ngOnInit() {


    this.configData.getSelection('PopulationByAges', 'Category')
      .subscribe(value => {
        this.categorySelection = value;
        this.selectedCategory = this.categorySelection[0].value;
      });
    this.configData.getSelection('PopulationByAges', 'Year')
      .subscribe(value => {
      this.yearSelection = value;
        this.selectedYear = this.yearSelection[0].value;
      });

    this.loadChartData(this.selectedCategory, this.selectedYear);
  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory(categorySelection) {
    this.selectedCategory = categorySelection.value;
    this.mapJsonToPopulationDataChart();
  }

  private loadChartData(selectedCategory: string, selectedYear: string) {
    this.jsonDB.callAPI(this.CATEGORY_API_URL).subscribe(response => {
      this.populaceData = response;
      this.mapJsonToPopulationDataChart();
    });
  }

  private mapJsonToPopulationDataChart() {
    this.chartData = [];
    this.chartLabels = [];
    // construct fieldname in googlesheets/json, remove minus(-),  JSON field name must not contain -
    const property = 'Label_' + this.selectedCategory.replace('-', '');

    // find all JSON with selected  year
    const filteredData = this.populaceData
      .filter(value => (value['Year'] == this.selectedYear))
      // remove Auckland and New Zealand
      .filter(value => value['Location'] != 'New Zealand' && value['Location'] != 'Auckland');
    // Copy location to array of labels
    filteredData.forEach(value => (this.chartLabels.push(value['Location'])));
    // Copy the values to chart data
    const data = [];
    filteredData.forEach(value => (data.push(value[property])));
    this.chartData.push({ 'data': data, 'label': String(this.selectedYear) });

    // console.log(JSON.stringify(this.chartLabels));
    // console.log(JSON.stringify(this.chartData));
  }
}


