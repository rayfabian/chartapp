import { OnInit, Component, ElementRef, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';
import { MatTableModule, MatInputModule, MatFormFieldModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { GraphDataBaseService } from '../services/graph-data-base.service';
import { ChartItem } from '../services/models/chart-item';
import { GraphDataSourceService } from '../services/graph-data-source.service';
import { SearchParam } from '../services/models/search-param';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css'],
  providers: [GraphDataBaseService, GraphDataSourceService]
})
export class DatatableComponent implements OnInit {

  dataSource: GraphDataSourceService;

  constructor(private http: Http, private graphDataBaseService: GraphDataBaseService, private _dataSource: GraphDataSourceService) {
    this.dataSource = this._dataSource;
    this.dataSource.filterYear = "2016";
  }

  selectedValue: string;



  items: ChartItem[] = [];
  setupYears: any[] = [];
  errorMessage: string;

  yearOptions = [];
  populationData = [];

  displayedColumns = ['id', 'source', 'label1', 'label2'];

  @ViewChild('filterYear') filterYear: ElementRef;


  ngOnInit() {

    this.getYearSetup();
    this._dataSource.filterYear = '2016';

    Observable.fromEvent(this.filterYear.nativeElement, 'change')
      .debounceTime(150)
      .distinctUntilChanged()
      .subscribe(() => {
        //  console.log(this.filterYear.nativeElement.value);
        if (!this._dataSource) { return; }
        this.dataSource.filterYear = this.filterYear.nativeElement.value;
      });


  }

  private getYearSetup() {
    this.graphDataBaseService.YearSetup
      .subscribe(
      res => {
        this.yearOptions = res.map(function (e) {
          return { id: e[0], value: e[1] };
        });
      //  console.log(JSON.stringify(this.yearOptions));
      },
      err => this.errorMessage = <any>err);
  }

  clicked(event) {
    console.log(this.dataSource.show());
  }
}
