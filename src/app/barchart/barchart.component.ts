import { Component, OnInit } from '@angular/core';
import { GraphDatabaseJsonserviceService } from '../services/graph-database-jsonservice.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-barchart',
  templateUrl: './barchart.component.html',
  styleUrls: ['./barchart.component.css'],
  providers: [GraphDatabaseJsonserviceService]
})
export class BarchartComponent implements OnInit {

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };





  public barChartLabels: string[] = ['Rodney', 'Hibiscus and Bays', 'Upper Harbour', 'Kaipatiki ', 'Devonport-Takapuna ',
    'Henderson-Massey ', 'Waitakere Ranges ', 'Great Barrier ',
    'Waiheke ', 'Waitemata', 'Whau ', 'Albert-Eden ', 'Puketapapa ',
    'Orakei ', 'Maungakiekie-Tamaki ', 'Howick ', 'Mangere-Otahuhu ', 'Otara-Papatoetoe ', 'Manurewa ',
    'Papakura ',
    'Franklin '];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;


  public tempChartData: any[] = [];
  public barChartData: any[] ;
  // public barChartData: any[] = [
  //   {
  //     data: [57300, 94000, 56800, 87000, 58500, 113500, 50700, 950, 8630, 81300, 76700, 100000,
  //       56300, 83700, 73700, 135000, 75300, 80300, 87000, 48200, 68300], label: '2013'
  //   }
  //   // { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
  //   // { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series C' }
  // ];






  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  public randomize(): void {
    console.log(JSON.stringify(this.tempChartData));
   // this.barChartData = this.tempChartData;
    // console.log(JSON.stringify(this.barChartData));
    // // Only Change 3 values
    // let data = [
    //   Math.round(Math.random() * 100),
    //   59,
    //   80,
    //   (Math.random() * 100),
    //   56,
    //   (Math.random() * 100),
    //   40];
    // let clone = JSON.parse(JSON.stringify(this.barChartData));
    // clone[0].data = data;
    // this.barChartData = clone;
    // /**
    //  * (My guess), for Angular to recognize the change in the dataset
    //  * it has to change the dataset variable directly,
    //  * so one way around it, is to clone the data, change it and then
    //  * assign it;
    //  */
  }

  constructor(private jsonDB: GraphDatabaseJsonserviceService) {
    // this.jsonDB.getPopulationByAgeCategory()
    // .subscribe(res => {
    //   this.tempChartData = res;
    // }
    // );

    
  }

  ngOnInit() {
    this.jsonDB.getJSON('poulation-by-ages')
    .toPromise().then(res => { this.barChartData = res; });
  }



}
