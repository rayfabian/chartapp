import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
  MatTabsModule,
  MatTableModule,
  MatSelectModule,
  MatExpansionModule,
  MatInputModule,
  MatFormFieldModule
} from '@angular/material';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { ChartsModule } from 'ng2-charts';
import { LinechartComponent } from './linechart/linechart.component';
import { TabsComponent } from './tabs/tabs.component';
import { BarchartComponent } from './barchart/barchart.component';
import { DatatableComponent } from './datatable/datatable.component';
import { PopulationComponent } from './population/population.component';
import { PopulationByAgeComponent } from './population-by-age/population-by-age.component';
import { PopulationByAgeComparisonComponent } from './population-by-age-comparison/population-by-age-comparison.component';
import { PopulationYearlyComparisonComponent } from './population-yearly-comparison/population-yearly-comparison.component';
import { PopulationByMigrationComponent } from './population-by-migration/population-by-migration.component';

@NgModule({
  declarations: [
    AppComponent,
    LinechartComponent,
    TabsComponent,
    BarchartComponent,
    DatatableComponent,
    PopulationComponent,
    PopulationByAgeComponent,
    PopulationByAgeComparisonComponent,
    PopulationYearlyComparisonComponent,
    PopulationByMigrationComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule,
    CommonModule,
    ChartsModule,
    MatTabsModule,
    MatTableModule,
    MatExpansionModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
