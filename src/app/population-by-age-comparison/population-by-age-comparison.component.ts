import { Component, OnInit } from '@angular/core';
import { GraphDatabaseJsonserviceService } from '../services/graph-database-jsonservice.service';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material';
import { ChartConfigService } from '../services/chart-config-service.service';

@Component({
  selector: 'app-population-by-age-comparison',
  templateUrl: './population-by-age-comparison.component.html',
  styleUrls: ['./population-by-age-comparison.component.css'],
  providers: [GraphDatabaseJsonserviceService, ChartConfigService]
})
export class PopulationByAgeComparisonComponent implements OnInit {

  private CATEGORY_URL = 'population-by-ages/';
  private CATEGORY_API_URL
  = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/population-by-age/1pPOMrGJUBfRjFzFfjjfG-k7XuEUO11FKQIiota4XFKo';
  populaceData: any[];

  yearSelection = [];
  categorySelection = [];

  selectedYear: string;
  selectedCategory1: string;
  selectedCategory2: string;

  chartDataSets = [];
  chartLabels: any[];
  chartType: string = 'bar';
  chartLegend: boolean = true;
  chartData: any[];


  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private jsonDB: GraphDatabaseJsonserviceService, private configData: ChartConfigService) { }

  ngOnInit() {

    this.configData.getSelection('PopulationByAges-Compare', 'Category')
      .subscribe(value => {
        this.categorySelection = value;
        this.selectedCategory1 = this.categorySelection[0].value;
        // set defualts to the 2nd
        this.selectedCategory2 = this.categorySelection[1].value;

      });
    this.configData.getSelection('PopulationByAges-Compare', 'Year')
      .subscribe(value => {
        this.yearSelection = value;
        this.selectedYear = this.yearSelection[0].value;
      });

    this.loadChartData();
  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory1(categorySelection1) {
    this.selectedCategory1 = categorySelection1.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory2(categorySelection2) {
    this.selectedCategory2 = categorySelection2.value;
    this.mapJsonToPopulationDataChart();
  }

  private loadChartData() {
    this.jsonDB.callAPI(this.CATEGORY_API_URL).subscribe(response => {
      this.populaceData = response;
      this.mapJsonToPopulationDataChart();
    });
  }

  private mapJsonToPopulationDataChart() {
    this.chartData = [];
    this.chartLabels = [];
    // construct labels based on horizontal data
    const selectedData1 = this.populaceData.
      filter(value => (value['Location'] == this.selectedCategory1 && value['Year'] == this.selectedYear))

    const selectedData2 = this.populaceData
      .filter(value => (value['Location'] == this.selectedCategory2 && value['Year'] == this.selectedYear))

    if (selectedData1.length > 0) {
      const data1 = [
        selectedData1[0]['Label_Total'], selectedData1[0]['Label_014'], selectedData1[0]['Label_1539'],
        selectedData1[0]['Label_4064'], selectedData1[0]['Label_65']
      ];
      this.chartData.push({ 'data': data1, 'label': this.selectedCategory1 });
    }

    if (selectedData2.length > 0) {
      const data2 = [
        selectedData2[0]['Label_Total'], selectedData2[0]['Label_014'], selectedData2[0]['Label_1539'],
        selectedData2[0]['Label_4064'], selectedData2[0]['Label_65']
      ];
      this.chartData.push({ 'data': data2, 'label': this.selectedCategory2 });

    }
    this.chartLabels = ['Total', '0-14', '15-39', '40-64', '65'];

    // add chart data    


    console.log(JSON.stringify(this.chartData));
    console.log(JSON.stringify(this.chartLabels));
  }
}

/**
 *   const yearList = this.populaceData.map(item => item['Year'])
      .filter((value, index, self) => self.indexOf(value) === index);
    console.log(JSON.stringify(yearList));
 */
