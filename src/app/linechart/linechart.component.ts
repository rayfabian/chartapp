import { Component, OnInit } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { GraphDataBaseService } from '../services/graph-data-base.service';
import { GraphDataSourceService } from '../services/graph-data-source.service';
import { ChartItem } from '../services/models/chart-item';
import { GraphDatabaseJsonserviceService } from '../services/graph-database-jsonservice.service';

@Component({
  selector: 'app-linechart',
  templateUrl: './linechart.component.html',
  styleUrls: ['./linechart.component.css'],
  providers: [GraphDataBaseService, GraphDataSourceService, GraphDatabaseJsonserviceService]
})

export class LinechartComponent implements OnInit {


  // lineChart
  public lineChartData: Array<any> = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Auckland' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Wellington' },
    { data: [18, 48, 77, 9, 100, 27, 40], label: 'Christchurch' },
    { data: [28, 48, 67, 9, 19, 27, 40], label: 'Hamilton' },
    { data: [28, 48, 67, 9, 19, 27, 40], label: 'Whangarei' },
    { data: [28, 48, 67, 9, 19, 27, 40], label: 'Palmerston North' },
    { data: [28, 48, 67, 9, 19, 27, 40], label: 'Queenstown' }
  ];
  public lineChartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions: any = {
    responsive: true
  };
  public lineChartColors: Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend: boolean = true;
  public lineChartType: string = 'line';


  private testArray: Array<any> = [];

  constructor(private graphDataBaseService: GraphDataBaseService, private dataSource: GraphDataSourceService,
    private jsonDB: GraphDatabaseJsonserviceService) {
    this.dataSource.filterYear = '2016';
  }


  ngOnInit() {
    let data = { 'chartData': this.lineChartData, 'chartLabels': this.lineChartLabels }
    //console.log(JSON.stringify(data));
    //console.log(JSON.stringify(this.lineChartData));


    // console.log(JSON.stringify(this.jsonDB.getPopulationByAgeCategory()));
  }

  /*public lineChartData: Array<any> = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Auckland' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Wellington' },
    { data: [18, 48, 77, 9, 100, 27, 40], label: 'Christchurch' },
    { data: [28, 48, 67, 9, 19, 27, 40], label: 'Hamilton' },
    { data: [28, 48, 67, 9, 19, 27, 40], label: 'Whangarei' },
    { data: [28, 48, 67, 9, 19, 27, 40], label: 'Palmerston North' },
    { data: [28, 48, 67, 9, 19, 27, 40], label: 'Queenstown' }
  ];
  */

  private convertToChart() {
    // let setupYears = ['2013', '2014', '2015', '2016', '2017'];
    // this.graphDataBaseService.PopulationData.
    //   subscribe(response => {
    //     const result: ChartItem[] = response.filter(r => r.label2 = '2016');
    //     const _lineChartData: Array<any> = new Array(result.length);
    //     result.

    //   });
  }

  public randomize(): void {
    let _lineChartData: Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = { data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label };
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}

