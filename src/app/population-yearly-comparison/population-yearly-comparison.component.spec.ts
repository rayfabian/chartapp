import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopulationYearlyComparisonComponent } from './population-yearly-comparison.component';

describe('PopulationYearlyComparisonComponent', () => {
  let component: PopulationYearlyComparisonComponent;
  let fixture: ComponentFixture<PopulationYearlyComparisonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopulationYearlyComparisonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopulationYearlyComparisonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
