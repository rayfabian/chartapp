import { Component, OnInit } from '@angular/core';
import { GraphDatabaseJsonserviceService } from '../services/graph-database-jsonservice.service';
import { CommonModule } from '@angular/common';
import { MatGridListModule } from '@angular/material';
import { ChartConfigService } from '../services/chart-config-service.service';


@Component({
  selector: 'app-population-yearly-comparison',
  templateUrl: './population-yearly-comparison.component.html',
  styleUrls: ['./population-yearly-comparison.component.css'],
  providers: [GraphDatabaseJsonserviceService, ChartConfigService]
})
export class PopulationYearlyComparisonComponent implements OnInit {

  private CATEGORY_URL = 'population-yearly/';
  private CATEGORY_API_URL
  = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/population-yearly/13IiIllmyS2HBBsYFbR5eMf9jXBRJrK7qnPCAdZLukfk';
  populaceData: any[];

  categorySelection = [];

  selectedYear: string;
  selectedCategory1: string;
  selectedCategory2: string;

  chartDataSets = [];
  chartLabels: any[];
  chartType: string = 'line';
  chartLegend: boolean = true;
  chartData: any[];



  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private jsonDB: GraphDatabaseJsonserviceService, private configData: ChartConfigService) { }

  ngOnInit() {

    this.configData.getSelection('PopulationByAges-Compare', 'Category')
      .subscribe(value => {
        this.categorySelection = value;
        // console.log(JSON.stringify(this.categorySelection));
        this.selectedCategory1 = this.categorySelection[0].value;
        this.selectedCategory2 = this.categorySelection[1].value;
      });

    this.loadChartData();
  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory1(categorySelection1) {
    this.selectedCategory1 = categorySelection1.value;
    this.mapJsonToPopulationDataChart();
  }

  public changeCategory2(categorySelection2) {
    this.selectedCategory2 = categorySelection2.value;
    this.mapJsonToPopulationDataChart();
  }

  private loadChartData() {
    this.jsonDB.callAPI(this.CATEGORY_API_URL).subscribe(
      response => {
        this.populaceData = response;
        this.mapJsonToPopulationDataChart();
      });
  }

  private mapJsonToPopulationDataChart() {
    this.chartData = [];
    this.chartLabels = [];

    // Find all years
    const yearList = this.populaceData.map(item => item['Year'])
      .filter((value, index, self) => self.indexOf(value) === index);
    // console.log(JSON.stringify(yearList));

    // find all location and year using first  category
    const selectedData1 = this.populaceData.
      filter(value => (value['Location'] == this.selectedCategory1 && yearList.includes(value['Year'])));

    // console.log(JSON.stringify(selectedData1));
    const data1 = [];
    selectedData1.forEach(value => {
      data1.push(value['Label_population']);
    });

    if (selectedData1.length > 0) {
      this.chartData.push({ 'data': data1, 'label': this.selectedCategory1 });
    }


    // find all location and year using second category
    const selectedData2 = this.populaceData.
      filter(value => (value['Location'] == this.selectedCategory2 && yearList.includes(value['Year'])));

    // console.log(JSON.stringify(selectedData2));
    const data2 = [];
    selectedData2.forEach(value => {
      data2.push(value['Label_population']);
    });

    if (selectedData2.length > 0) {
      this.chartData.push({ 'data': data2, 'label': this.selectedCategory2 });
    }

    // put the year in the labels
    yearList.forEach(value => {
      this.chartLabels.push(String(value));
    });

    console.log(JSON.stringify(this.chartData));
    console.log(JSON.stringify(this.chartLabels));
  }
}
