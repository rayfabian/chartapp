import { Injectable } from '@angular/core';
import { GraphDatabaseJsonserviceService } from './graph-database-jsonservice.service';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ChartConfigService {
  private CONFIG_URL
  = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/configuration/1Dd3JIdHB8mKVax1t2Dyig1d2ZPiDHzKQGtmJ5Prarnc';

  constructor(private http: Http) { }

  public getSelection(chart: string, selection: string): Observable<any[]> {
    return this.http
      .get(this.CONFIG_URL)
      .map(res => res.json()
        .filter(value => value['chart'] == chart && value['selection'] == selection)
      ).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occured', error);
    return Promise.reject(error.messagen || error);
  }

}
