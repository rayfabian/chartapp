import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';


const DATA_ROOT_URL = './assets/data/';

@Injectable()
export class GraphDatabaseJsonserviceService {


  constructor(private http: Http) { }

  public getJSON(strQuery: string): Observable<Array<any>> {
    return this.http.get(DATA_ROOT_URL + strQuery + '.json')
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

  public callAPI(url: string): Observable<Array<any>> {
    return this.http.get(url)
      .map((res: Response) => res.json())
      .catch(this.handleError);
  }

}
