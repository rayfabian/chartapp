import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ChartItem } from './models/index';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

const POPULATION_DATA_URL = './assets/data/population.csv';
const YEAR_SETUP_DATA_URL = './assets/data/year-setup.csv';


@Injectable()
export class GraphDataBaseService {

  dataChange: BehaviorSubject<ChartItem[]> = new BehaviorSubject<ChartItem[]>([]);
  get data(): ChartItem[] { return this.dataChange.value; }

  errorMessage: string;
  constructor(private http: Http) { }


  get PopulationData(): Observable<ChartItem[]> {
    return this.getData(POPULATION_DATA_URL)
      .map(this.convertToChartData)
      .catch(this.handleError);
  }

  public getPopulation(searchKey: string): Observable<ChartItem[]> {
    return this.getData(POPULATION_DATA_URL)
      .map(this.convertToChartData)
      .catch(this.handleError);
  }

  private populateData() {
    this.PopulationData.toPromise().then(
      res => {
        const copiedData = this.data.slice();
        res.forEach(element => {
          copiedData.push(element);
          // console.log(JSON.stringify(element));
        });
        this.dataChange.next(copiedData);
        // console.log(JSON.stringify(this.dataChange.value));
      }
    ).catch(err => {
      console.log(err);
    });
  }

  private filterData(searchKey: string, data: ChartItem[]) {
    return data.every(e => e.label2 === searchKey);
  }

  private convertToChartData(result: ChartItem[]) {
    const chartData =
      result.map(function (e) {
        let item = new ChartItem();
        item.id = e[0];
        item.source = e[1];
        item.label1 = e[2];
        item.label2 = e[3];
        return item;
      });
    // console.log('Convert Data' + JSON.stringify(chartData));
    return chartData;
  }

  get YearSetup(): Observable<any[]> {
    return this.getData(YEAR_SETUP_DATA_URL);
  }

  private getData(strURL: string): Observable<any[]> {
    return this.http.get(strURL)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let csvData = res['_body'] || '';
    let allTextLines = csvData.split(/\r\n|\n/);
    let headers = allTextLines[0].split(',');
    let lines = [];

    for (let i = 0; i < allTextLines.length; i++) {
      // split content based on comma
      let data = allTextLines[i].split(',');
      if (data.length == headers.length) {
        let tarr = [];
        for (let j = 0; j < headers.length; j++) {
          tarr.push(data[j]);
        }
        lines.push(tarr);
      }
    }
    return lines;
  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }


}
