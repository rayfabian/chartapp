import { Component, OnInit } from '@angular/core';
import { GraphDatabaseJsonserviceService } from '../services/graph-database-jsonservice.service';
import { ChartConfigService } from '../services/chart-config-service.service';

@Component({
  selector: 'app-population-by-migration',
  templateUrl: './population-by-migration.component.html',
  styleUrls: ['./population-by-migration.component.css'],
  providers: [GraphDatabaseJsonserviceService, ChartConfigService]
})
export class PopulationByMigrationComponent implements OnInit {

  private CATEGORY_API_URL
  = 'https://s3.amazonaws.com/chartdata.cometauckland.org.nz/population-by-migration/1RgK3s7pcEQVpcuw1TNejdcBuaTOJYRLIAkrpuYflUzc';

  populaceData: any[];
  yearSelection = [];
  selectedYear: string;

  chartDataSets = [];
  chartLabels: any[];
  chartType: string = 'bar';
  chartLegend: boolean = true;
  chartData: any[];



  public chartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      xAxes: [{
        stacked: true,
      }],
      yAxes: [{
        stacked: true
      }]
    }


  };

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }

  constructor(private jsonDB: GraphDatabaseJsonserviceService, private configData: ChartConfigService) { }

  ngOnInit() {

    this.configData.getSelection('PopulationByMigration', 'Year')
      .subscribe(value => {
        this.yearSelection = value;
        this.selectedYear = this.yearSelection[0].value;
      });


    this.loadChartData();
  }

  public changeYear(yearSelection) {
    this.selectedYear = yearSelection.value;
    this.mapJsonToPopulationDataChart();
  }


  private loadChartData() {
    this.jsonDB.callAPI(this.CATEGORY_API_URL).subscribe(response => {
      this.populaceData = response;
      this.mapJsonToPopulationDataChart();
    });
  }

  private mapJsonToPopulationDataChart() {
    this.chartData = [];
    this.chartLabels = [];
    // Select data based on the Year selected
    const selectedData = this.populaceData.filter(value => (value['Year'] == this.selectedYear))
    // Exclude New Zealand and Auckland
    const filtereData = selectedData.filter(value => (value['Location'] != 'New Zealand' && value['Location'] != 'Auckland'));
    // console.log(JSON.stringify(selectedData));
    const data1 = [];
    const data2 = [];
    // Put all labels using Location property and get the data1 and data2
    filtereData.forEach(value => {
      this.chartLabels.push(value['Location']);
      data1.push(value['Label_naturalincrease']);
      data2.push(value['Label_netmigration']);
    });
    // Construct chart data
    this.chartData.push({ 'data': data1, 'label': 'Natural Increase' });
    this.chartData.push({ 'data': data2, 'label': 'Net Migration' });

    console.log(JSON.stringify(this.chartData));
    console.log(JSON.stringify(this.chartLabels));
  }
}
