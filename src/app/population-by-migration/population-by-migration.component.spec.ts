import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopulationByMigrationComponent } from './population-by-migration.component';

describe('PopulationByMigrationComponent', () => {
  let component: PopulationByMigrationComponent;
  let fixture: ComponentFixture<PopulationByMigrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopulationByMigrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopulationByMigrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
